<?php

class DNAFactory_ImprovedPicking_Model_Observer
{
    public function addMassActions($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) != 'Mage_Adminhtml_Block_Widget_Grid_Massaction' || $block->getRequest()->getControllerName() != 'sales_order') {
            return;
        }

        $block->addItem('export_aggregated_Excel', array(
           'label' => 'Export Aggregated Excel',
            'url' => Mage::app()->getStore()->getUrl('adminhtml/sales_orders_exporter/aggregate')
        ));

        $block->addItem('export_detailed_Excel', array(
            'label' => 'Export Detailed Excel',
            'url' => Mage::app()->getStore()->getUrl('adminhtml/sales_orders_exporter/detailed')
        ));
    }
}
