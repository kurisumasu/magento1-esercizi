<?php

class DNAFactory__Helper_Management_Files_ExcelManagement extends Mage_Core_Helper_Abstract
{
    public function arrayToXml($rows)
    {
        $excelXml = '<table border="1">';
        if (!$rows || !is_array($rows)) {
            return "";
        }

        $row = array_pop($rows);
        foreach ($row as $key => $cell) {
            $excelXml.= "<th>$key</th>";
        }

        foreach ($rows as $col) {
            $excelXml.= "<tr>";
            foreach ($col as $cell) {
                $excelXml.= "<td align='center'>$cell</td>";
            }
            $excelXml.= "</tr>";
        }

        $excelXml .= '</table>';
        return $excelXml;
    }
}
