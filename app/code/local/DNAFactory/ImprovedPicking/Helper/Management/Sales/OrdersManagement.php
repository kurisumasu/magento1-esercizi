<?php

class DNAFactory_ImprovedPicking_Helper_Management_Sales_OrdersManagement extends Mage_Core_Helper_Abstract
{

    public function createAggregateArray($orders)
    {
        $aggregateArray = array();
        try {
            if (!$orders || !$orders->getFirstItem()) {
                return array();
            }

            $products = array();
            foreach ($orders as $order) {
                foreach ($order->getAllVisibleItems() as $product) {
                    $products[] = $product;
                }
            }

            $aggregateArray = $this->groupBy($products, "sku");
        } catch (Exception $e) {
            return array();
        }

        return $aggregateArray;
    }

    public function createDetailedArray($orders)
    {
        $detaildedArray = array();
        try {
            if (!$orders || !$orders->getFirstItem()) {
                return array();
            }

            foreach ($orders as $order) {
                if (!$order->getShippingAddress()) {
                    continue;
                }

                $shippingAddress = strip_tags($order->getShippingAddress()->getFormated(false));
                $shippingAddress = preg_replace( "/\r|\n/", " ", $shippingAddress);

                $detailedOrder = array();
                $detailedOrder["shipping"] = $shippingAddress;
                $detailedOrder["increment_id"] = $order->getId();

                $aggregateArray = $this->groupBy($order->getAllVisibleItems(), "sku");

                foreach ($aggregateArray as $aggregateOrder) {
                    $detaildedArray[] = array_merge($aggregateOrder, $detailedOrder);
                }
            }
        } catch (Exception $e) {
            return array();
        }
        return $detaildedArray;
    }

    protected function groupBy($items, $key = "id", $includeAllData = false)
    {
        if (!$items) {
            return array();
        }

        $results = array();
        foreach ($items as $item) {
            if (!$item->hasData($key)) {
                continue;
            }

            if (!array_key_exists($item->getData($key), $results) || !array_key_exists("qty", $results[$item->getSku()])) {
                $results[$item->getData($key)]["qty"] = 0;
                $results[$item->getData($key)][$key] = $item->getData($key);

                if ($includeAllData) {
                    $results[$item->getData($key)]["data"] = $item->getData();
                }
            }
            $results[$item->getData($key)]["qty"]+= (int)$item->getData("qty_ordered");
        }
        return $results;
    }

}
