<?php

class DNAFactory_ImprovedPicking_Adminhtml_Sales_Orders_ExporterController extends Mage_Adminhtml_Controller_Action
{

    public function aggregateAction()
    {
        $ordersManagement = Mage::helper("dnafactory_improvedpicking/management_sales_ordersManagement");
        $excelManagement = Mage::helper("dnafactory_improvedpicking/management_files_ExcelManagement");

        try {

            $itemIds = $this->getMassActionItems();

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('entity_id', array('in' => $itemIds))
                ->load();

            $aggregateOrders = $ordersManagement->createAggregateArray($orders);
            $xml = $excelManagement->arrayToXml($aggregateOrders);

            $this->downloadXmlFile($xml, "aggregate");

        } catch(Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirectReferer();
    }

    public function detailedAction()
    {
        $ordersManagement = Mage::helper("dnafactory_improvedpicking/management_sales_ordersManagement");
        $excelManagement = Mage::helper("dnafactory_improvedpicking/management_files_ExcelManagement");

        try {
            $itemIds = $this->getMassActionItems();

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('entity_id', array('in' => $itemIds))
                ->load();

            $detailedArray = $ordersManagement->createDetailedArray($orders);
            $xml = $excelManagement->arrayToXml($detailedArray);

            $this->downloadXmlFile($xml, "detailed");

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirectReferer();
    }

    protected function downloadXmlFile($xml, $fileName = "file")
    {
        if (!$xml) {
            return false;
        }

        header ("Content-Type: application/vnd.ms-excel");
        header ("Content-Disposition: inline; filename=$fileName.xls");
        echo $xml;

        return true;
    }

    protected function getMassActionItems()
    {
        $params = $this->getRequest()->getParams();
        $datakey = $params['massaction_prepare_key'];
        $itemIds = $params[$datakey];

        if (!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
            $this->_redirectReferer();
        }

        return $itemIds;
    }
}
