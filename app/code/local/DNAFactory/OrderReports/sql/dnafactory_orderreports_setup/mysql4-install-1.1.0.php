<?php

$installer = $this;
$installer->startSetup();

$reportPaymentAmountsTable = $installer->getConnection()
    ->newTable($installer->getTable("dnafactory_orderreports/sales_order_report_payment_amount"))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
    ), 'entity_id')
    ->addColumn('date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
    ), 'report date');

$reportPaymentAmountsMethodTable = $installer->getConnection()
    ->newTable($installer->getTable("dnafactory_orderreports/sales_order_report_payment_amount_method"))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
    ), 'entity_id')
    ->addColumn('report_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'report reference')
    ->addColumn('payment_method', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ), 'payment method')
    ->addColumn('total', Varien_Db_Ddl_Table::TYPE_DECIMAL, null, array(
        'scale'     => 2,
        'precision' => 10,
        'nullable'  => false,
        'default'   => 0
    ), 'orders total of this payment method')
    ->addForeignKey(
        $installer->getFkName("dnafactory_orderreports/sales_order_report_payment_amount_method", "report_id", "dnafactory_orderreports/sales_order_report_payment_amount", "entity_id"),
        'report_id',
        $installer->getTable("dnafactory_orderreports/sales_order_report_payment_amount"),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$installer->getConnection()->createTable($reportPaymentAmountsTable);
$installer->getConnection()->createTable($reportPaymentAmountsMethodTable);

$installer->endSetup();