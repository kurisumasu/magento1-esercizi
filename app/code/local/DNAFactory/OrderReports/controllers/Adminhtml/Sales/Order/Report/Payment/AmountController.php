<?php

class DNAFactory_OrderReports_Adminhtml_Sales_Order_Report_Payment_AmountController extends Mage_Adminhtml_Controller_Report_Abstract
{
    public function indexAction()
    {
        $this->loadLayout();

        $gridBlock = $this->getLayout()->getBlock('adminhtml_sales_order_report_payment_amount_grid.grid');
        $filterFormBlock = $this->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction(array(
            $gridBlock,
            $filterFormBlock
        ));

        $this->renderLayout();
    }

    public function exportPaymentAmountExcelAction()
    {
        $reportManagement = Mage::helper("dnafactory_orderreports/management_sales_order_report_payment_amountManagement");
        $excelManagement = Mage::helper("dnafactory_orderreports/management_files_excelManagement");

        $encodedParams = $this->getRequest()->getParam('filter', null);
        if (!$encodedParams) {
            $this->_getSession()->addError($this->__('Please press "Show Report" before export'));
            $this->_redirectReferer();
            return;
        }

        $requestData = Mage::helper('adminhtml')->prepareFilterString($encodedParams);
        if (!$requestData || !is_array($requestData) || empty($requestData) || !array_key_exists("from", $requestData) || !array_key_exists("to", $requestData)) {
            $this->_getSession()->addError($this->__('Please press "Show Report" before export'));
            $this->_redirectReferer();
            return;
        }

        $fromDate = $requestData["from"];
        $toDate = $requestData["to"];

        $includeEmptyDates = $requestData["include_empty_dates"];
        if (!$includeEmptyDates) {
            $includeEmptyDates = false;
        }

        $fromDate = Mage::getModel('core/date')->date('Y/m/d', $fromDate);
        $toDate = Mage::getModel('core/date')->date('Y/m/d', $toDate);


        $aggregateReports = $reportManagement->getArray($fromDate, $toDate, $includeEmptyDates);
        $xml = $excelManagement->arrayToXml($aggregateReports);

        $config = Mage::helper("dnafactory_orderreports/config_order_reports_general");
        $fileName = $config->getConfig("excel_file_name");

        if (!$fileName) {
            $fileName = "file";
        }

        $this->downloadXmlFile($xml, $fileName);
    }

    protected function downloadXmlFile($xml, $fileName = "file")
    {
        if (!$xml) {
            return false;
        }

        header ("Content-Type: application/vnd.ms-excel");
        header ("Content-Disposition: inline; filename=$fileName.xls");
        echo $xml;

        return true;
    }
}
