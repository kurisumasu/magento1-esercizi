<?php

class DNAFactory_OrderReports_Block_Adminhtml_Sales_Order_Report_Payment_Amount_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dnafactory_orderreports";
        $this->_controller = 'adminhtml_sales_order_report_payment_amount_grid';
        $this->_headerText = Mage::helper('reports')->__('Payment Amount Report');
        parent::__construct();

        $this->_removeButton('add');
        $this->addButton('filter_form_submit', array(
            'label'     => Mage::helper('reports')->__('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ));
    }
}
