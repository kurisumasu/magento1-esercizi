<?php

class DNAFactory_OrderReports_Block_Adminhtml_Sales_Order_Report_Payment_Amount_Filter_Form extends Mage_Adminhtml_Block_Report_Filter_Form
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof Varien_Data_Form_Element_Fieldset) {

            $fieldset->addField('include_empty_dates', 'select', array(
                'name'      => 'include_empty_dates',
                'label'     => Mage::helper('reports')->__('Include Empty Dates'),
                'options'   => array(
                    '0' => Mage::helper('reports')->__('No'),
                    '1' => Mage::helper('reports')->__('Yes'),
                ),
                'required'  => true
            ));
        }

        return $this;
    }
}