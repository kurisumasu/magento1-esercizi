<?php

class DNAFactory_OrderReports_Block_Adminhtml_Sales_Order_Report_Payment_Amount_Grid_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dnafactory_payment_amount_report_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        parent::_prepareCollection();

        $management = Mage::helper("dnafactory_orderreports/management_sales_order_report_payment_amountManagement");

        $filterData = $this->getFilterData();
        $fromDate = $filterData->getFrom();
        $toDate = $filterData->getTo();
        $includeEmptyDates = $filterData->getIncludeEmptyDates();

        if (!$includeEmptyDates) {
            $includeEmptyDates = false;
        }

        $collection = $management->getGridCollection($fromDate, $toDate, $includeEmptyDates);

        $this->setCollection($collection);
        $this->addColumns();
        return $this;
    }

    protected function addColumns()
    {
        $management = Mage::helper("dnafactory_orderreports/management_sales_order_report_payment_amountManagement");
        $paymentMethods = $management->getAllPaymentMethodsFromCollection($this->getCollection());

        if (!$paymentMethods || !is_array($paymentMethods)) {
            return parent::_prepareColumns();
        }

        foreach ($paymentMethods as $paymentMethod) {
            $index = $management->getIndexByPaymentMethodName($paymentMethod);
            $this->addColumn($index, array(
                'header' => $paymentMethod,
                'index' => $index
            ));
        }

        return parent::_prepareColumns();
    }

    protected function _prepareColumns()
    {
        $this->addExportType('*/*/exportPaymentAmountExcel', $this->__('Excel XML'));

        $this->addColumn('date', array(
            'header'    =>$this->__('Date'),
            'index'     =>'date'
        ));
        return parent::_prepareColumns();
    }
}
