<?php

class DNAFactory_OrderReports_Model_Resource_Sales_Order_Report_Payment_Amount extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('dnafactory_orderreports/sales_order_report_payment_amount', 'entity_id');
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        $select->joinLeft(
            array("amount_table" => $this->getTable('dnafactory_orderreports/sales_order_report_payment_amount_method')),
            $this->getMainTable().'.entity_id = amount_table.report_id',
            array('*')
        );
        return $select;
    }

}
