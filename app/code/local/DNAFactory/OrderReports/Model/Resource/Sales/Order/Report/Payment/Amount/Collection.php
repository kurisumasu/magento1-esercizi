<?php

class DNAFactory_OrderReports_Model_Resource_Sales_Order_Report_Payment_Amount_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dnafactory_orderreports/sales_order_report_payment_amount');
    }

    protected function _afterLoad()
    {
        $select = $this->getSelect();
        $select->joinLeft(
            array("amount_table" => $this->getTable('dnafactory_orderreports/sales_order_report_payment_amount_method')),
            'main_table.entity_id = amount_table.report_id',
            array('*')
        );

        return parent::_afterLoad();
    }
}
