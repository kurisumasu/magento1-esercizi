<?php

class DNAFactory_OrderReports_Model_Resource_Sales_Order_Report_Payment_Amount_Method_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dnafactory_orderreports/sales_order_report_payment_amount_method');
    }
}
