<?php

class DNAFactory_OrderReports_Model_Sales_Order_Report_Payment_Amount extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dnafactory_orderreports/sales_order_report_payment_amount');
    }

    public function loadByDate($date)
    {
        if (!$date) {
            return $this;
        }

        $date = date('Y-m-d', strtotime($date . "+1 day"));
        if (!$date) {
            return $this;
        }
        $date = Mage::getModel('core/date')->date('Y/m/d', $date);

        $paymentAmount = $this->getCollection()
            ->addFieldToFilter('date', array(
            'from'     => $date,
            'to'       => $date,
            'datetime' => true
        ));

        if (!$paymentAmount || !$paymentAmount->getFirstItem()) {
            return null;
        }
        return $paymentAmount;
    }
}
