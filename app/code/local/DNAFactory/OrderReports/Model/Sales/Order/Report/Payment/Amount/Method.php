<?php

class DNAFactory_OrderReports_Model_Sales_Order_Report_Payment_Amount_Method extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dnafactory_orderreports/sales_order_report_payment_amount_method');
    }
}
