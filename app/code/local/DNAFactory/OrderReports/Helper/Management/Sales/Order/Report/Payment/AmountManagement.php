<?php

class DNAFactory_OrderReports_Helper_Management_Sales_Order_Report_Payment_AmountManagement extends Mage_Core_Helper_Abstract
{
    static $_paymentMethods = null;

    public function getArray($fromDate, $toDate, $includeEmptyDates = false)
    {
        $period = new DatePeriod(
            new DateTime($fromDate),
            new DateInterval('P1D'),
            new DateTime($toDate)
        );

        // questo foreach rallenta tantissimo l esecuzione
        $paymentsAmounts = array();
        foreach ($period as $day) {
            $day = $day->format('Y-m-d');

            if ($this->alreadyExists($day)) {
                $paymentAmount = $this->getArrayFromDB($day);
            } else {
                $paymentAmount = $this->createAndSave($day);
            }
            $paymentAmount = $this->formatPrice($paymentAmount);

            if (!$paymentAmount || !is_array($paymentAmount) || empty($paymentAmount)) {
                if (!$includeEmptyDates) {
                    continue;
                }
                $paymentAmount = ["date" => $day];
            }

            $paymentsAmounts[] = $paymentAmount;
        }
        return $paymentsAmounts;
    }

    protected function formatPrice($paymentAmount)
    {
        if (!$paymentAmount || !is_array($paymentAmount) || empty($paymentAmount)) {
            return $paymentAmount;
        }

        foreach ($paymentAmount as $key => $field) {
            if ($key === "date") {
                continue;
            }

            $paymentAmount[$key] =  $formattedPrice = Mage::helper('core')
                ->currency($field,true,false);
        }

        return $paymentAmount;
    }

    protected function createAndSave($day)
    {
        if (!$day) {
            return array();
        }
        $paymentAmount = $this->create($day);

        if (!$paymentAmount || !is_array($paymentAmount) || empty($paymentAmount)) {
            return array();
        }

        if ($this->canBeSaved($day)) {
            $this->save($paymentAmount);
        }
        return $paymentAmount;
    }

    /*Dovrebbe andare nel repository?*/
    protected function save($report)
    {
        $report = $this->parseArrayToSave($report,"date");

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $date = $report["date"];
        try {
            $connection->beginTransaction();

            $paymentAmount = Mage::getModel("dnafactory_orderreports/sales_order_report_payment_amount")
                ->setDate($date)
                ->save();

            $reportId = $paymentAmount->getId();
            foreach ($report["paymentMethods"] as $paymentMethod) {

                Mage::getModel("dnafactory_orderreports/sales_order_report_payment_amount_method")
                    ->setPaymentMethod($paymentMethod["title"])
                    ->setReportId($reportId)
                    ->setTotal($paymentMethod["amount"])
                    ->save();

            }
            $connection->commit();
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $connection->rollBack();
            return false;
        }

        return true;
    }

    protected function create($day)
    {
        //incremento di 1 giorno perchè magento diminuisce di un giorno durante la conversione
        //utilizzo zend perchè più semplice e preciso per il fine di prendere ordini dal db
        $fromDate = new Zend_Date($day);
        $fromDate = $fromDate->toString("yyyy-MM-dd HH:mm:ss");

        $toDate = new Zend_Date($day);
        $toDate->addMinute(1439);
        $toDate = $toDate->toString("yyyy-MM-dd HH:mm:ss");

        if (!$day) {
            return array();
        }

        $orders = Mage::getModel("sales/order")->getCollection()
            ->addFieldToFilter('created_at', array(
                'from'     => $fromDate,
                'to'       => $toDate,
                'datetime' => true
            ))
            ->addAttributeToFilter('state', Mage_Sales_Model_Order::STATE_COMPLETE);

        if (!$orders || !$orders->getFirstItem()) {
            return array();
        }

        $results = array();
        $aggregateOrders = $this->groupBy($orders, "created_at", "this");

        if (!$aggregateOrders || !is_array($aggregateOrders)) {
            return array();
        }

        foreach ($aggregateOrders as $key => $aggregateOrder) {
            if (!array_key_exists("this", $aggregateOrder)) {
                continue;
            }

            //sommo tutti i total degli ordini per ogni giorno
            $aggregateByPayments = array();
            foreach ($aggregateOrder["this"] as $order) {
                $arrayKey = $order->getPayment()->getMethodInstance()->getTitle();

                if (!array_key_exists($arrayKey, $aggregateByPayments)) {
                    $aggregateByPayments[$arrayKey] = 0;
                }
                $aggregateByPayments[$arrayKey]+= $order->getGrandTotal();
            }
            unset($aggregateOrders[$key]['this']);

            if (!array_key_exists($key, $aggregateOrders)) {
                continue;
            }
            $results = array_merge($aggregateOrders[$key], $aggregateByPayments);
        }

        if (!$results || !is_array($results)) {
            return array();
        }

        $results['date'] = $results['created_at'];
        unset($results['created_at']);
        return $results;
    }

    protected function canBeSaved($day)
    {
        //salvo solamente i giorni che presentano tutti gli ordini
        // completati o cancellati e quindi non modificabili
        $fromDate = new Zend_Date($day);
        $fromDate = $fromDate->toString("yyyy-MM-dd HH:mm:ss");

        $toDate = new Zend_Date($day);
        $toDate->addMinute(1439);
        $toDate = $toDate->toString("yyyy-MM-dd HH:mm:ss");

        if (!$toDate || !$fromDate) {
            return false;
        }

        $completedOrders = Mage::getModel("sales/order")->getCollection()
            ->addFieldToFilter('created_at', array(
                'from'     => $fromDate,
                'to'       => $toDate,
                'datetime' => true
            ))
            ->addFieldToFilter(
                array("state", "state"),
                array(Mage_Sales_Model_Order::STATE_COMPLETE, Mage_Sales_Model_Order::STATE_CANCELED)
            );

        $allOrders = Mage::getModel("sales/order")->getCollection()
            ->addFieldToFilter('created_at', array(
                'from'     => $fromDate,
                'to'       => $toDate,
                'datetime' => true
            ));


        $completedOrdersCount = $completedOrders->count();
        $allOrdersCount = $allOrders->count();
        if ($completedOrdersCount < $allOrdersCount) {
            return false;
        }
        return true;
    }

    protected function getArrayFromDB($day)
    {
        $paymentAmount = Mage::getModel("dnafactory_orderreports/sales_order_report_payment_amount")->loadByDate($day);
        
        $composedArray = $this->modelToParsedArray($paymentAmount->getData());
        if (!$composedArray) {
            //ritorno null per fare il controllo nella funzione chiamante
            return null;
        }
        return $composedArray;
    }
    
    protected function groupBy($items, $dataKey = "id", $sumKey = null)
    {
        if (!$items) {
            return array();
        }

        $results = array();

        foreach ($items as $item) {

            if (!$item->hasData($dataKey)) {
                continue;
            }

            //se si tratta di date raggruppo per giorno
            if ($this->isValidDate($item->getData($dataKey))) {
                //incremento perchè magento diminuisce di una data
                $date = date('Y/m/d', strtotime($item->getData($dataKey) . "+1 day"));
                $arrayKey = Mage::getModel('core/date')->date('Y/m/d', $date);
            } else {
                $arrayKey = $item->getData($dataKey);
            }

            if (!array_key_exists($arrayKey, $results)) {
                $results[$arrayKey][$dataKey] = $arrayKey;
                if ($sumKey != null) {
                    if (is_numeric($arrayKey)) {
                        $results[$arrayKey][$sumKey] = 0;
                    } else {
                        $results[$arrayKey][$sumKey] = array();
                    }
                }
            }
            if ($sumKey != null) {
                if (is_numeric($arrayKey)) {
                    $results[$arrayKey][$sumKey] += (double)$item->getData($sumKey);;
                } else {
                    if ($sumKey != "this") {
                        $results[$arrayKey][$sumKey][] = $item->getData($sumKey);
                    } else {
                        $results[$arrayKey][$sumKey][] = $item;
                    }
                }
            }
        }
        return $results;
    }

    protected function isValidDate($date)
    {
        if (!$date) {
            return false;
        }

        try {
            new DateTime($date);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
     * this function transform a collection->getData()
     * in a parsed array for exel stamp
     */
    protected function modelToParsedArray($reports)
    {
        if (!is_array($reports)) {
            return array();
        }

        $semiComposedArray = array();
        foreach ($reports as $key => $report) {
            if (!array_key_exists("payment_method", $report) && !array_key_exists("total", $report)) {
                continue;
            }

            $paymentMethod = $report["payment_method"];
            $total = $report["total"];

            $report[$paymentMethod] = $total;

            //rimuovo i field che non mi servono
            unset($report["entity_id"]);
            unset($report["payment_method"]);
            unset($report["total"]);
            unset($report["report_id"]);

            $semiComposedArray[] = $report;
        }

        $composedArray = array();
        foreach ($semiComposedArray as $report) {
            if (!array_key_exists("date", $report)) {
                continue;
            }

            foreach ($report as $key => $reportField) {
                $composedArray[$key] = $reportField;
            }
        }

        return $composedArray;
    }

    protected function parseArrayToSave($report, $dateKey)
    {
        if (!$report || !is_array($report) || !$dateKey) {
            return array();
        }

        $parsedReport = array();
        if (!array_key_exists($dateKey, $report)) {
            return array();
        }

        $date = $report[$dateKey];
        unset($report[$dateKey]);

        $paymentMethods = array();
        foreach ($report as $paymentMethod => $amount) {
            $paymentMethods[$paymentMethod]["title"] = $paymentMethod;
            $paymentMethods[$paymentMethod]["amount"] = $amount;
        }
        $parsedReport["date"] = $date;
        $parsedReport["paymentMethods"] = $paymentMethods;

        return $parsedReport;
    }

    protected function alreadyExists($date)
    {
        $paymentsAmounts = Mage::getModel("dnafactory_orderreports/sales_order_report_payment_amount")->loadByDate($date);

        if (!$paymentsAmounts || $paymentsAmounts->count() < 1) {
            return false;
        }

        return true;
    }

    public function getAllPaymentMethodsFromCollection($collection)
    {
        $paymentMethods = array();

        foreach ($collection as $report)
        {
            $reportData = $report->getData();

            foreach ($reportData as $key => $field) {
                if ($key === "date") {
                    continue;
                }
                $paymentMethods[] = $key;
            }
        }

        $paymentMethods = array_unique($paymentMethods);

        if (!$paymentMethods || !is_array($paymentMethods)) {
            return array();
        }
        return $paymentMethods;
    }

    public function getAllSavedPaymentMethods()
    {
        if (self::$_paymentMethods) {
            return self::$_paymentMethods;
        }

        try {
            $paymentsAmounts = Mage::getModel("dnafactory_orderreports/sales_order_report_payment_amount_method")->getCollection();
            $paymentMethods = array();

            foreach ($paymentsAmounts as $paymentsAmount) {
                $paymentMethods[] = $paymentsAmount->getPaymentMethod();
            }

            self::$_paymentMethods = array_unique($paymentMethods);
            return self::$_paymentMethods;
        } catch(Exception $e) {
            return array();
        }
    }

    /*
     * get the collection for print records in a grid
     */
    public function getGridCollection($fromDate, $toDate, $includeEmptyDates = false)
    {
        $paymentsAmount = $this->getArray($fromDate, $toDate, $includeEmptyDates);
        $paymentsAmountCollection = $this->arrayToGridCollection($paymentsAmount);

        if (!$paymentsAmountCollection || !$paymentsAmountCollection->getFirstItem()) {
            return new Varien_Data_Collection();
        }
        return $paymentsAmountCollection;
    }

    protected function arrayToGridCollection($items) {
        // è lecito create con new degli oggetti varien?
        $collection = new Varien_Data_Collection();
        foreach ($items as $item) {

            foreach ($item as $key => $field) {
                if ($key === 'date') {
                    continue;
                }

                $parsedKey = $this->getIndexByPaymentMethodName($key);
                $item[$parsedKey] = $field;
                unset($item[$key]);
            }

            $varienObject = new Varien_Object();
            $varienObject->setData($item);
            $collection->addItem($varienObject);
        }
        return $collection;
    }

    public function getIndexByPaymentMethodName($paymentMethodName) {
        $paymentMethodName = str_replace(' ', '', $paymentMethodName);
        $paymentMethodIndex = str_replace('/', '', $paymentMethodName);

        return $paymentMethodIndex;
    }
}
