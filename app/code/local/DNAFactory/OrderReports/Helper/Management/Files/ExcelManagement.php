<?php

class DNAFactory_OrderReports_Helper_Management_Files_ExcelManagement extends Mage_Core_Helper_Abstract
{
    public function arrayToXml($rows)
    {
        $excelXml = '<table border="1">';
        if (!$rows || !is_array($rows)) {
            return "";
        }

        $keys = $this->getAllKeys($rows);

        foreach ($keys as $key) {
            $excelXml.= "<th>$key</th>";
        }

        foreach ($rows as $col) {
            $excelXml.= "<tr>";
            foreach ($keys as $key) {
                if (array_key_exists($key, $col)) {
                    $excelXml .= "<td align='center'>$col[$key]</td>";
                } else {
                    $excelXml .= "<td align='center'></td>";
                }
            }
            $excelXml.= "</tr>";
        }

        $excelXml .= '</table>';
        return $excelXml;
    }

    protected function getAllKeys($rows)
    {
        $keys = array();
        foreach ($rows as $row) {
            $keys = array_merge($keys, array_keys($row));
        }

        $keys = array_unique($keys);
        return $keys;
    }
}
