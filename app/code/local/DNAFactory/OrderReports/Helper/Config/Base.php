<?php

abstract class DNAFactory_OrderReports_Helper_Config_Base extends Mage_Core_Helper_Abstract
{
    protected $XML_STORE_CONFIG = "null/null/%s";

    public function getConfig($field, $store = null)
    {
        if (!$store) {
            try {
                $store = Mage::app()->getStore();
            } catch (Exception $e) {
                $store = null;
            }
        }

        return trim(Mage::getStoreConfig(sprintf($this->XML_STORE_CONFIG, $field), $store));
    }
}
